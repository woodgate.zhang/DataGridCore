import empty from "is-empty";

let {Keystore, Keygen} = require('eosjs-keygen');
import fs from 'fs';
import path from 'path';
import assert from 'assert';
import EOS from 'eosjs';
import ecc from 'eosjs-ecc';

Keygen.generateMasterKeys().then(keys => {
    // create blockchain account called 'myaccount'
    console.log(keys)
});


const eosConfig = {
    chainId: '038f4b0fc8ff18a4f0842a8f0564611f6e96e8535901dd45e43ac8691a1c4dca', // Jungle Testnet  http://dev.cryptolions.io:38888/v1/chain/get_info
    keyProvider: ['5K1ZE5seSS3PdxWHjXsRreqtTBEjMBPrL4WTke6Fe7jwf17rHKK'], // existing account (active) private key that has ram cpu and bandwidth already purchased
    httpEndpoint: 'http://193.93.219.219:8888', // jungle testnet
    expireInSeconds: 60,
    broadcast: true,
    debug: true,
    sign: true
};
let eos = EOS(eosConfig);
/*
eos.getInfo({}).then(result => {
    console.log(result);
}).catch(error => {
    console.log(error);
});
*/
eos.getAccount("mingqi").then(result => {
    console.log(JSON.stringify(result));
}).catch(error => {
    console.log(error.toString());
});
/*
const privateKey = '5JMSam9PCaXGaaB9R4og7YExBYVmVJgXBPAvuGLaGb66N6vWJjk';
const publicKey = ecc.privateToPublic(privateKey);
console.log("pub key: " + publicKey);

const rawMsg = '{"name": "2.jpg", "desc":"spaceX second launch", "location": "Kennedey"}';



let msg = ecc.sha256(rawMsg);
console.log("msg: " + msg);
const sig = ecc.sign(msg, privateKey);
console.log("signature: " + sig);

const recoveredPublicKey = ecc.recover(sig, msg); // get public key
console.log("pub key: " + recoveredPublicKey);

const isValidTX = ecc.verify(sig, msg, publicKey);
console.log("verified: ", isValidTX);
*/
/*
let signedHash = ecc.signHash(ecc.sha256(msg), privateKey);
console.log("signedHash:" + signedHash);

const publicKey = ecc.recover(sig, msg.trim()); // get public key
console.log(publicKey);
*/

/*
const bufferFile = fs.readFileSync(path.resolve(__dirname, '../../resource/cat.jpg'));
//console.log(bufferFile.toString("hex"));
let encryptedMessage = ecc.Aes.encrypt(privateKey, publicKey, bufferFile);
console.log(encryptedMessage);
fs.writeFileSync(path.resolve(__dirname, '../../resource/cat-encrypted.txt'), encryptedMessage.message);

const bufferEncryptedMessage = fs.readFileSync(path.resolve(__dirname, '../../resource/cat-encrypted.txt'));

console.log(bufferEncryptedMessage);
console.log( bufferEncryptedMessage.nonce );

//const decryptedMessage = ecc.Aes.decrypt(privateKey, publicKey, encryptedMessage.nonce, encryptedMessage.message, encryptedMessage.checksum);
//fs.writeFileSync('cat-bk.png', decryptedMessage);
*/

/*
let encryptedTxt = ecc.Aes.encrypt(privateKey, publicKey, msg);
console.log(encryptedTxt);
const decryptedTxt = ecc.Aes.decrypt(privateKey, publicKey, encryptedTxt.nonce, encryptedTxt.message, encryptedTxt.checksum);
console.log(decryptedTxt.toString())
*/