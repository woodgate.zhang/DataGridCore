"use strict";
require('dotenv').config();

import EOS from 'eosjs';
import { Keygen } from 'eosjs-keygen';
import { UnknownEOSAccountError, EOSNetworkConnectionError } from "../other/CustomErrors";
import Config from '../../../config/config';

const Debug = require('debug')('DataGrid:core:eos-util');

class EOSUtil {
    static generateMasterKeys() {
        return new Promise((resolve, reject) => {
            Keygen.generateMasterKeys().then(keys => {
                Debug(keys);
                resolve(keys);
            }).catch(error => {
                reject(error);
            });
        });
    }
    constructor() {
        Debug("EOSUtil > constructor");
        this.eosConfig = {
            expireInSeconds: 60,
            broadcast: true,
            debug: true,
            sign: true
        }
    }
    init(host, port, chainId, keyProvider) {
        this.eosConfig.chainId = chainId;
        this.eosConfig.keyProvider = keyProvider;
        this.eosConfig.httpEndpoint = 'http://' + host + ':' + port;
        this.eos = EOS(this.eosConfig);
    }
    getInfo() {
        this.eos.getInfo({}).then(result => {
            Debug(result);
        }).catch(error => {
            Debug(error);
        });
    }
    getAccount(accountName) {
        Debug("getAccount > %s", accountName);
        return new Promise((resolve, reject) => {
            this.eos.getAccount(accountName).then(account => {
                Debug(JSON.stringify(account));
                resolve(account);
            }).catch(error => {
                if(error.message.includes('unknown key'))
                    reject(new UnknownEOSAccountError("unknown account name"));
                else
                    reject(new EOSNetworkConnectionError("failed to connect to network, check host or port"));
            });
        });
    }
    getTableRows(contractName, accountName, tableName, lowerBound="", upperBound="", limit=10) {
        Debug("getTableRows > %s, %s, %s", contractName, accountName, tableName);
        return new Promise((resolve, reject) => {
            this.eos.getTableRows({
                code: contractName,
                scope: accountName,
                table: tableName,
                lower_bound: lowerBound,
                upper_bound: upperBound,
                limit: limit,
                json: true,
            }).then(function (res) {
                console.log(res);
                resolve(res);
            }).catch(error => {
                Debug(error.toString());
                reject(error);
            })
        });
    }
}

const eosUtil = new EOSUtil();

Debug("eos network type: %s", process.env.EOS);

if(process.env.EOS === 'test') {
    eosUtil.init(Config.eos.test.host, Config.eos.test.port, Config.eos.test.chainId, Config.eos.test.keyProvider);
} else if(process.env.EOS === 'main') {
    eosUtil.init(Config.eos.main.host, Config.eos.main.port, Config.eos.main.chainId, Config.eos.main.keyProvider);
}

eosUtil.getInfo();

export { EOSUtil, eosUtil };
