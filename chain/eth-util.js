import Web3 from 'web3';
import utils from 'ethereumjs-util';

class ETHUtil {
    static recover(signature, message) {
        try {
            const r = utils.toBuffer(signature.slice(0, 66));
            const s = utils.toBuffer('0x' + signature.slice(66, 130));
            const v = utils.bufferToInt(utils.toBuffer('0x' + signature.slice(130, 132)));
            const m = utils.toBuffer(message);
            return utils.ecrecover(m, v, r, s).toString('hex');
        } catch(error) {
            throw error;
        }
    }
    static pubToAddress(pubKey, sanitize=false) {
        try {
            return utils.publicToAddress(pubKey, sanitize);
        } catch(error) {
            throw error;
        }
    }
    constructor() {
        this.web3 = new Web3();
    }
    sha3(strData) {
        return this.web3.utils.sha3(strData);
    }
    signedSha3(strData) {
        return this.web3.utils.sha3("\x19Ethereum Signed Message:\n" + strData.length + strData);
    }
}

const test = () => {
    const web3 = new Web3();
    const msg = "Hello, world!";
    const signedMsg = web3.eth.accounts.sign(msg, '0x04f60ce3d41707bf8907abff5e962c09a8bf4e35a571f4ee51984aace96c27f0');
    console.log(signedMsg);
    const pub = ETHUtil.recover(signedMsg.signature, signedMsg.messageHash);
    console.log(pub);
    const address = ETHUtil.pubToAddress(Buffer.from(pub, 'hex'));
    console.log(address.toString('hex'));
};

//test();

const ethUtil = new ETHUtil();

export { ETHUtil, ethUtil };
