'use strict';

class FilteredMetadata {
    constructor(metadata) {
        this._id = metadata._id;
        this.hashId = metadata.hashId;
        this.account = metadata.account;
        this.size = metadata.size;
        this.timestamp = metadata.timestamp;
        this.extra = metadata.extra;
    }
}

export {
    FilteredMetadata
};