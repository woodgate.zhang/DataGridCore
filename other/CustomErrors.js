'use strict';

function UnknownEOSAccountError(message) {
    this.name = 'UnknownAccountNameError';
    this.message = message || '';
    let error = new Error(this.message);
    error.name = this.name;
    this.stack = error.stack;
}
UnknownEOSAccountError.prototype = Object.create(Error.prototype);
function EOSNetworkConnectionError(message) {
    this.name = 'EOSNetworkConnectionError';
    this.message = message || '';
    let error = new Error(this.message);
    error.name = this.name;
    this.stack = error.stack;
}
EOSNetworkConnectionError.prototype = Object.create(Error.prototype);


export { UnknownEOSAccountError, EOSNetworkConnectionError }