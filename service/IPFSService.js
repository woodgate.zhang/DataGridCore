'use strict';

require("babel-core/register");
require("babel-polyfill");

import empty from "is-empty";
import { ipfsClient as ipfs } from '../ipfs/IPFSClient';

const Debug = require('debug')('DataGrid:core:service:ipfs');

class IPFSService {
    constructor() {
        this.ipfs = ipfs;
    }
    resetConnect(multiAddr) {
        this.ipfs.resetWithMultiAddr(multiAddr);
    };
    ls(req, res, next) {
        Debug("ls >");
        Debug("hashId: %s", req.params.hashId);
        if( empty(req.params.hashId) ) {
            res.send(400, { error: 'missing parameters' });
            return next();
        }
        this.ipfs.ls(req.params.hashId).then(files => {
            res.send(200, files);
            return next()
        }).catch(error => {
            res.send(500, error.message);
            return next();
        });
    }
    catFile(req, res, next) {
        Debug("cat >");
        Debug("hashId: %s", req.params.hashId);
        if( empty(req.params.hashId) ) {
            res.send(400, { error: 'missing parameters' });
            return next();
        }
        this.ipfs.catFile(req.params.hashId).then(ret => {
            res.send(200, ret);
            return next()
        }).catch(error => {
            res.send(500, error.message);
            return next();
        });
    }
    getFile(req, res, next) {
        Debug("get >");
        Debug("hashId: %s", req.params.hashId);
        if( empty(req.params.hashId) ) {
            res.send(400, { error: 'missing parameters' });
            return next();
        }
        this.ipfs.getFile(req.params.hashId).then(files => {
            res.send(200, files);
            return next();
        }).catch(error => {
            res.send(500, error.message);
            return next();
        });
    }
    statObject(req, res, next) {
        Debug("get >");
        Debug("hashId: %s", req.params.hashId);
        if( empty(req.params.hashId) ) {
            res.send(400, { error: 'missing parameters' });
            return next();
        }
        this.ipfs.statObject(req.params.hashId).then(stats => {
            res.send(200, stats);
            return next();
        }).catch(error => {
            res.send(500, error.message);
            return next();
        });
    }
}

const ipfsService = new IPFSService(ipfs);

export  {
    ipfsService
};