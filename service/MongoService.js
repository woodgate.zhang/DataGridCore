'use strict';

import empty from "is-empty";
import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';

const Debug = require('debug')('DataGrid:core:service:mongo');

const metadataSchema = new mongoose.Schema({
    block    :  { type: Number,  required: true },
    thxId    :  { type: String, required: true, index: true },
    account  :  { type: String, required: true, index: true },
    hashId   :  { type: String, required: true, unique: true },
    size     :  { type: Number, min: 0, required: true },
    timestamp:  { type: Date, default: Date.now, index: true },
    extra    :  { type: Map, of: String }
});
metadataSchema.plugin(mongoosePaginate);
const MetadataTHX = mongoose.model('metadata-thx', metadataSchema);

Debug("created schema 'MetadataTHX'!");

class MongoService {
    static addNewMetadataTHX(jsonData)  {
        Debug("addNewMetadataTHX>");
        return new Promise((resolve, reject) => {
            const metadataTHX = new MetadataTHX(jsonData);
            metadataTHX.save().then(ret => {
                console.log("addNewMetadataTHX> " + JSON.stringify(ret));
                resolve(ret);
            }).catch(error => {
                Debug("addNewMetadataTHX> %s", error.toString());
                reject(error);
            })
        });
    };
    static deleteThenAddMetadataTHX(jsonData)  {
        Debug("deleteThenAddMetadataTHX>");
        return new Promise((resolve, reject) => {
            MetadataTHX.deleteOne({ hashId: jsonData.hashId }, function (err) {
                if(err)
                    Debug(err.toString());
                const metadataTHX = new MetadataTHX(jsonData);
                metadataTHX.save().then(ret => {
                    Debug("deleteThenAddMetadataTHX> " + JSON.stringify(ret));
                    resolve(ret);
                }).catch(error => {
                    Debug("deleteThenAddMetadataTHX> ", error.toString());
                    reject(error);
                })
            });
        });
    };
    static updateMetadata(query, updateBody, options={new:true,runValidators:true}) {
        return new Promise((resolve, reject) => {
            MetadataTHX.findOneAndUpdate(query, updateBody, options, function (err, ret) {
                if (err) {
                    Debug(err.toString());
                    reject(err);
                }
                resolve(ret);
            });
        });
    }
    static find(query, pageNum=1, limitNum=2, sortField='timestamp', order=-1) {
        const options = {};
        const sortOptions = {};
        if(!empty(sortField) && !empty(order)) {
            sortOptions[sortField] = order;
        }
        if(!empty(pageNum) && !empty(limitNum)) {
            options.page = pageNum;
            options.limit = limitNum;
            if(!empty(sortOptions)) {
                options.sort = sortOptions
            }
        }
        Debug("query option: %s", JSON.stringify(options));
        return new Promise((resolve, reject) => {
            MetadataTHX.paginate(query, options, function(err, result) {
                if (err) {
                    Debug(err.toString());
                    reject(err);
                }
                resolve(result);
            });
        });
    };
}
export {
    MetadataTHX, MongoService
};