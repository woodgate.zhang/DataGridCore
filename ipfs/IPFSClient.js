'use strict';

import ipfsAPI from "ipfs-api";

import Config from '../../../config/config';

const Debug = require('debug')('DataGrid:core:ipfs:IPFSClient');

class IPFSClient {
    constructor(host, port=5001, options={protocol: 'http'}) {
        Debug("IPFSClient > constructor");
        this.host = host;
        this.port = port;
        this.options = options;
    }
    init() {
        Debug("IPFSClient > init");
        console.log(this.options);
        try {
            this.ipfsAPI = ipfsAPI(this.host, this.port, this.options);
        } catch(error) {
            throw error;
        }
    }
    getIPFS() {
        return this.ipfsAPI;
    }
    resetWithMultiAddr(multiAddr) {
        try {
            this.ipfsAPI = ipfsAPI(multiAddr);
        } catch(error) {
            throw error;
        }
    }
    getId() {
        return new Promise((resolve, reject) => {
            this.ipfsAPI.id((err, identity) => {
                if (err) {
                    reject(err);
                }
                resolve(identity);
            })
        });
    }
    getVersion() {
        return new Promise((resolve, reject) => {
            this.ipfsAPI.version((err, version) => {
                if (err) {
                    reject(err);
                }
                resolve(version);
            })
        });
    }
    ls(hashId) {
        Debug("ls ", hashId);
        return new Promise((resolve, reject) => {
            this.ipfsAPI.ls('/ipfs/' + hashId).then(files => {
                Debug(files);
                resolve(files);
            }).catch(error => {
                reject(error);
            })
        });
    }
    catFile(hashId) {
        return new Promise((resolve, reject) => {
            this.ipfsAPI.files.cat('/ipfs/' + hashId).then(file => {
                Debug(file);
                resolve(file);
            }).catch(error => {
                reject(error);
            })
        });
    }
    getFile(hashId) {
        return new Promise((resolve, reject) => {
            this.ipfsAPI.files.get('/ipfs/' + hashId).then(file => {
                Debug(file);
                resolve(file);
            }).catch(error => {
                reject(error);
            })
        });
    }
    statObject(hashId) {
        return new Promise((resolve, reject) => {
            this.ipfsAPI.object.stat(hashId).then(stats => {
                Debug(stats);
                resolve(stats);
            }).catch(error => {
                reject(error);
            })
        });
    }
    addFile(data, options) {
        Debug("addFiles > file buffer size: %s", data.length);
        return new Promise((resolve, reject) => {
            this.ipfsAPI.add(data, options).then(files => {
                resolve(files);
            }).catch(error => {
                reject(error);
            })
        });
    }
}

const ipfsClient = new IPFSClient(Config.ipfs.host, Config.ipfs.port, Config.ipfs.options);

ipfsClient.init();

ipfsClient.getId().then(id => {
    Debug(id);
}).catch(error => {
    Debug(error);
});

export {ipfsClient};