'use strict';

import IPFSFactory from 'ipfsd-ctl';
/* Spawn IPFS daemons using JavaScript
 * Batteries not included. Bring your own IPFS executable
 * - ipfs - > npm i ipfs - If you want to spawn js-ipfs nodes and/or daemon
 */
module.exports = IPFSFactory.create({type: 'js'});